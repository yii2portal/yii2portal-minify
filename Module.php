<?php

namespace yii2portal\minify;

use Yii;
use yii\base\Event;
use yii\web\Response;


class Module extends \yii2portal\core\Module
{


    public function init()
    {
        parent::init();

        if(!YII_DEBUG) {
            Yii::$app->response->on(Response::EVENT_BEFORE_SEND, function (Event $event) {
                $response = $event->sender;
                if ($response->format === Response::FORMAT_HTML) {
                    if (!empty($response->data)) {
                        $response->data = self::compress($response->data);
                    }
                    if (!empty($response->content)) {
                        $response->content = self::compress($response->content);
                    }
                }
            });
        }

    }

    private static function compress($html)
    {
        $filters = array(
            '#(\s){2,}#iu' => '$1',

        );
        $output = preg_replace(array_keys($filters), array_values($filters), $html);
        return trim($output);
    }
}